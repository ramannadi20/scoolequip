import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './mystyle.module.css';


class ScoolEquip extends Component {

    constructor(props) {
        super(props);
        this.state = { sports: "Sports", Fname: "Student First Name:", SchName: "School Name:", stationary: "Stationary" };
    }
    
    enterYouInfo = () => {
      window.location.href = 'http://www.google.com/';
    }

    render() {
        return (
            <div>
                <h1 className={styles.h1} >ScoolEquip</h1>
                <h2 className={styles.h2} >One Stop for all your needs</h2>
                <table className={styles.tbl} >                  
                    <tr>
                        <td> {this.state.SchName}</td>
                        <input type="text" />
                    </tr>
                    <tr>
                        <td>{this.state.Fname} </td>
                        <input type="text" />
                    </tr>
                    <tr>
                        <td>{this.state.sports}</td>
                    </tr>
                    <tr>
                        <td>{this.state.stationary}</td>
                    </tr>
                </table>
                <button
                    type="button"
                    onClick={this.enterYouInfo}
                >Enter Your Info</button>

            </div>);
    }
}


class Schools extends Component {
    render() {
        return (
            <div>
                <ScoolEquip />
            </div>
        );
    }
}


export default Schools;