import React from 'react';
import ReactDOM from 'react-dom';
import Schools from './App.js';
import './App.css';

ReactDOM.render(<Schools />, document.getElementById('root'));